<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 2:45 AM
 */   ?>

<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/15/2017
 * Time: 11:22 AM
 */

$error = "";
$success = "";
if($_SERVER['REQUEST_METHOD']=="POST"){




    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel');


    $filename=$_FILES["csv"]["tmp_name"];



    if($_FILES["csv"]["size"] > 0 && in_array($_FILES['csv']['type'],$csvMimes))
    {


        $file = fopen($filename, "r");


        $data = array();

        $keywords = "";
        $tags = "";
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {

            if(!empty($keywords)){

                $data[$i][$keywords] = $getData[0];
                $data[$i][$tags] =$getData[1];
                $i++;
            }
            if($getData[0] = "keywords")
                $keywords = $getData[0];
            if($getData[1] = "tags")
                $tags = $getData[1];

        }

        fclose($file);



        define('DB_SERVER', 'localhost');
        define('DB_USERNAME', 'root');
        define('DB_PASSWORD', '');
        define('DB_NAME', 'dashboard');

        /* Attempt to connect to MySQL database */
        try{
            $pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



            $stmt = $pdo->prepare("INSERT INTO csv_data_upload (keywords, tags)
    VALUES (:keywords, :tags)");

            foreach($data as $key=>$value){
                $stmt->bindParam(':keywords', $value['keywords']);
                $stmt->bindParam(':tags', $value['tags']);


                $stmt->execute();
            }

            $success ="data uploaded to  database  successfully : ";

        } catch(PDOException $e){
            die("ERROR: Could not connect. " . $e->getMessage());
        }

    }else{

        $error =  "not a csv file or file is empty! please select a valid file ";
    }




}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
    <!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">
    <style type="text/css">
        .error{
            padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
        .navbar-brand {
            color: #26617d;
            margin-left: 23%;
            margin-bottom: 2%;

        }

        .navbar-brand:hover{
            color: #4c99ab;
        }

        .navbar-brand img{
            display: inline-block;
        }
        hr{
            border-color: #4e9aac;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
</head>
<body>






<div class="container">
    <h1 class="welcome text-center"> </h1>
    <div class="card card-container">

        <!--<h2 class='login_title text-center'>Login</h2>-->
        <a href="/" class="navbar-brand">


        </a>
        <hr>
        <p id="message" class="text-center alert  alert-danger error" <?php if(empty($error)) echo "hidden"; ?> >   <?php if(!empty($error)) echo $error; ?></p>
        <p id=" " class="text-center alert  alert-success error" <?php if(empty($success)) echo "hidden"; ?> >   <?php if(!empty($success)) echo $success; ?></p>


        <form class="form-signin"   method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  >
            <span id="reauth-email" class="reauth-email"></span>
            <p class="input_title">Select CSV File To upload</p>
            <input type="file"  name="csv" class="login_box" placeholder="data.csv" required autofocus>



            <button class="btn btn-lg btn-primary" id="upload" type="submit">upload</button>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->

<script type="text/javascript" src="/js/jquery.min.js"></script>






</body>
</html>
