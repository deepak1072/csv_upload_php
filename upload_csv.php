<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 2:55 AM
 */
if($_SERVER['REQUEST_METHOD']=="POST"){


    $filename=$_FILES["csv"]["tmp_name"];


    if($_FILES["csv"]["size"] > 0)
    {
        $file = fopen($filename, "r");


        $data = array();

        $keywords = "";
        $tags = "";
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {

            if(!empty($keywords)){

                $data[$i][$keywords] = $getData[0];
                $data[$i][$tags] =$getData[1];
                $i++;
            }
            if($getData[0] = "keywords")
                $keywords = $getData[0];
            if($getData[1] = "tags")
                $tags = $getData[1];

        }

        fclose($file);

        define('DB_SERVER', 'localhost');
        define('DB_USERNAME', 'root');
        define('DB_PASSWORD', '');
        define('DB_NAME', 'dashboard');

        /* Attempt to connect to MySQL database */
        try{
            $pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



            $stmt = $pdo->prepare("INSERT INTO csv_data_upload (keywords, tags) 
    VALUES (:keywords, :tags)");

            foreach($data as $key=>$value){
                $stmt->bindParam(':keywords', $value['keywords']);
                $stmt->bindParam(':tags', $value['tags']);


                $stmt->execute();
            }

            echo "uploaded successfully : ";

        } catch(PDOException $e){
            die("ERROR: Could not connect. " . $e->getMessage());
        }

    }else{

        echo "file is empty! ";
    }




}else{
    echo "Something went wrong !";
}